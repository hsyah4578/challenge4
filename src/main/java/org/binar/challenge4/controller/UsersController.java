package org.binar.challenge4.controller;

import org.binar.challenge4.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class UsersController {

    @Autowired
    private UsersService usersService;

    public String addUser(String userName, String emailAddress, String password){
        usersService.saveUsers(userName, emailAddress, password);
        return "Add user sukses";
    }

    public String updateUserEmail (String userName, String newEmail){
        usersService.updateEmailUser(userName, newEmail);
        return "Update email sukses";
    }

    public String hapusUser (String userName){
        usersService.hapusUser(userName);
        return "Hapus user sukses";
    }
}
