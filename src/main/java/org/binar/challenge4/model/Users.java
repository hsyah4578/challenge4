package org.binar.challenge4.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Setter
@Getter
@Entity
public class Users {

    @Id
    private String userName;

    @Column(unique = true)
    private String emailAddress;

    private String password;

}
