package org.binar.challenge4.service;

public interface UsersService {

    void saveUsers (String userName, String emailAddress, String password);

    void updateEmailUser (String userName, String newEmail);

    void hapusUser (String userName);

}
