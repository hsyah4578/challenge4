package org.binar.challenge4.model;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
public class PesanTiket {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer nomorTiket;

    @ManyToOne
    @JoinColumn(name = "userName")
    private Users userName;

    @ManyToOne
    @JoinColumn(name = "scheduleId")
    private Schedules scheduleId;

    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "nomorKursi", referencedColumnName = "nomorKursi"),
            @JoinColumn(name = "studio", referencedColumnName = "studio")
    })
    private Seats pesan;
}
