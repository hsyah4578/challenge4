package org.binar.challenge4.model;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;

@Getter
@Setter
@Entity
public class Schedules {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer scheduleId;

    @ManyToOne
    @JoinColumn(name = "kodeFilm")
    private Films kodeFilm;

    private String tanggalTayang;

    private String jamMulai;

    private String jamSelesai;

    private Float hargaTiket;
}
