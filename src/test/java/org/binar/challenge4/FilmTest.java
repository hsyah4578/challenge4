package org.binar.challenge4;

import org.binar.challenge4.controller.FilmsController;
import org.binar.challenge4.model.Films;
import org.binar.challenge4.repository.SchedulesRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;

@SpringBootTest
public class FilmTest {

    @Autowired
    private FilmsController filmsController;

    @Test
    @DisplayName("Test menambah film")
    public void addFilm(){
        String response = filmsController.addFilm(
                "F01", "Blue","N");
        Assertions.assertEquals("Add film sukses", response);
    }

    @Test
    @DisplayName("Test update judul film")
    public void updateJudulByKodeFilm(){
        String response = filmsController.updateJudul("A01","Upin Ipin The Movie");
        Assertions.assertSame("Update judul film success",response);
    }

    @Test
    @DisplayName("Test hapus film")
    public void hapusFilm(){
        String response = filmsController.hapusFilm("F01");
        Assertions.assertSame("Hapus film sukses",response);
    }

    @Test
    @DisplayName("Test add jadwal film")
    public void addScheduleFilm(){
        Films film = new Films();
        film.setKodeFilm("A01");
        String response = filmsController.addSchedule(film,"2022-04-14","19.00","21.30",50000f);
        Assertions.assertSame("Add schedule film sukses",response);
    }

    @Test
    @DisplayName("Test show film yang tayang")
    public void getFilmsBySedangTayang() {
        filmsController.getFilmsBySedangTayang("Y");
    }

    @Test
    @DisplayName("Test show jadwal film")
    public void getJadwalFilm() {
        filmsController.getJadwal("Upin Ipin");
    }

}
