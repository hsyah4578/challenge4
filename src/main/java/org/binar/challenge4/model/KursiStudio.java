package org.binar.challenge4.model;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class KursiStudio implements Serializable {

    private Integer nomorKursi;
    private String studio;

}
