package org.binar.challenge4.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Setter
@Getter
@Entity
public class Films {

    @Id
    private String kodeFilm;

    private String judulFilm;

    @Column(length = 1)
    private String sedangTayang;

}
