package org.binar.challenge4.repository;

import org.binar.challenge4.model.Films;
import org.binar.challenge4.model.Schedules;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SchedulesRepository extends JpaRepository <Schedules, Integer> {

    @Query("select s from Schedules s "+
            "join Films f on f.kodeFilm = s.kodeFilm "+
            "where f.judulFilm = :judulFilm")
    List<Schedules> findJadwalFilm(@Param("judulFilm") String judulFilm);


}
