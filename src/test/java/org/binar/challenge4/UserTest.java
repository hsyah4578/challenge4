package org.binar.challenge4;

import org.binar.challenge4.controller.UsersController;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UserTest {

    @Autowired
    private UsersController usersController;

    @Test
    @DisplayName("Test add user")
    public void addUser(){
        String response = usersController.addUser(
                "HERMAN", "herman@binar.com","passbinar");
        Assertions.assertEquals("Add user sukses", response);
    }

    @Test
    @DisplayName("Test update email user")
    public void updateEmailUser(){
        String response = usersController.updateUserEmail(
                "HERMAN","new_herman@binar.com"
        );
        Assertions.assertSame("Update email sukses", response);
    }

    @Test
    @DisplayName("Test hapus user")
    public void hapusUser(){
        String response = usersController.hapusUser("HERMAN");
        Assertions.assertSame("Hapus user sukses",response);
    }
}
