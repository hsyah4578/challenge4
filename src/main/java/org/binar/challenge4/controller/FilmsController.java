package org.binar.challenge4.controller;

import org.binar.challenge4.model.Films;
import org.binar.challenge4.model.Schedules;
import org.binar.challenge4.service.FilmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class FilmsController {

    @Autowired
    private FilmsService filmsService;

    public String addFilm(String kodeFilm, String judulFilm, String sedangTayang) {
        filmsService.saveFilms(kodeFilm, judulFilm, sedangTayang);
        return "Add film sukses";
    }
    public String updateJudul(String kodeFilm, String judulFilm) {
        filmsService.updateJudul(kodeFilm, judulFilm);
        return "Update judul film success";
    }

    public String hapusFilm (String kodeFilm){
        filmsService.hapusFilm(kodeFilm);
        return "Hapus film sukses";
    }

    public String addSchedule (Films kodeFilm, String tanggalTayang,
                               String jamMulai, String jamSelesai, Float hargaTiket){
        filmsService.saveSchedule(kodeFilm, tanggalTayang, jamMulai, jamSelesai, hargaTiket);
        return "Add schedule film sukses";
    }

    public List<Films> getFilmsBySedangTayang(String sedangTayang) {
        List<Films> films = filmsService.getFilmsBySedangTayang(sedangTayang);
        System.out.println("Film yang sedang tayang :");
        films.forEach(film -> {
            System.out.println(film.getJudulFilm());
        });
        return films;
    }

    public List<Schedules> getJadwal (String judulFilm) {
        List<Schedules> schedules = filmsService.getJadwalFilm(judulFilm);
        System.out.println("Judul Film\t\tTanggal tayang\t\tJam mulai\t\tJam selesai");
        schedules.forEach(schedule -> {
            System.out.println(schedule.getKodeFilm().getJudulFilm()+"\t\t"+
                    schedule.getTanggalTayang()+"\t\t\t"+
                    schedule.getJamMulai()+"\t\t\t"+
                    schedule.getJamSelesai());
        });
        return schedules;
    }

}
