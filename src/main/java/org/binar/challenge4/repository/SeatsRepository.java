package org.binar.challenge4.repository;

import org.binar.challenge4.model.KursiStudio;
import org.binar.challenge4.model.Seats;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SeatsRepository extends JpaRepository<Seats, KursiStudio> {
}
