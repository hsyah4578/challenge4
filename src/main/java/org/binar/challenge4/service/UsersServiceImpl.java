package org.binar.challenge4.service;

import org.binar.challenge4.model.Users;
import org.binar.challenge4.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class UsersServiceImpl implements UsersService{
    @Autowired
    private UsersRepository usersRepository;

    @Override
    public void saveUsers(String user_name, String email_address, String password) {
        Users user = new Users();
        user.setUserName(user_name);
        user.setEmailAddress(email_address);
        user.setPassword(password);
        usersRepository.save(user);
    }

    @Override
    public void updateEmailUser(String user_name, String newEmail) {
        Users user = usersRepository.findByUserName(user_name);
        user.setEmailAddress(newEmail);
        usersRepository.save(user);
    }

    @Override
    @Transactional
    public void hapusUser(String user_name) {
        usersRepository.hapusUser(user_name);
    }
}
