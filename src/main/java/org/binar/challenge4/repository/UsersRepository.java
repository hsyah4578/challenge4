package org.binar.challenge4.repository;

import org.binar.challenge4.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepository extends JpaRepository<Users, String> {

    @Query("select u from Users u where u.userName = :userName")
    Users findByUserName (@Param("userName") String userName);

    @Modifying
    @Query("delete from Users u where u.userName = :userName")
    void hapusUser(@Param("userName") String userName);

}
