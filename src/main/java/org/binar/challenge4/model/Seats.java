package org.binar.challenge4.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Getter
@Setter
@Entity(name = "seats")
public class Seats {

    @EmbeddedId
    private KursiStudio kursiStudio;

    @Column(length = 1)
    private String booked;
}
