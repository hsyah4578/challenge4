package org.binar.challenge4.service;

import org.binar.challenge4.model.Films;
import org.binar.challenge4.model.Schedules;

import java.util.List;

public interface FilmsService {

    void saveFilms (String kodeFilm, String judulFilm, String sedangTayang);

    void updateJudul (String kodeFilm, String judulFilm);

    void hapusFilm (String kodeFilm);

    void saveSchedule (Films kodeFilm, String tanggalTayang, String jamMulai, String jamSelesai, Float hargaTiket );

    List<Films> getFilmsBySedangTayang(String sedangTayang);

    List<Schedules> getJadwalFilm(String judulFilm);
}
