package org.binar.challenge4.repository;

import org.binar.challenge4.model.Films;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FilmsRepository extends JpaRepository<Films, String> {

    @Query("select f from Films f where f.kodeFilm = :kodeFilm")
    Films findByKodeFilm (@Param("kodeFilm") String kodeFilm);

    @Modifying
    @Query("delete from Films f where f.kodeFilm = :kodeFilm")
    void hapusFilm(@Param("kodeFilm") String kodeFilm);

    @Query("select f from Films f where f.sedangTayang = :sedangTayang")
    List<Films> findFilmsBySedangTayang (@Param("sedangTayang") String sedangTayang);

}
