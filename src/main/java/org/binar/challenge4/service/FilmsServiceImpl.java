package org.binar.challenge4.service;

import org.binar.challenge4.model.Films;
import org.binar.challenge4.model.Schedules;
import org.binar.challenge4.repository.FilmsRepository;
import org.binar.challenge4.repository.SchedulesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class FilmsServiceImpl implements FilmsService {
    @Autowired
    private FilmsRepository filmsRepository;

    @Autowired
    private SchedulesRepository schedulesRepository;

    @Override
    public void saveFilms(String kodeFilm, String judulFilm, String sedangTayang) {
        Films film = new Films();
        film.setKodeFilm(kodeFilm);
        film.setJudulFilm(judulFilm);
        film.setSedangTayang(sedangTayang);
        filmsRepository.save(film);
    }

    @Override
    public void updateJudul(String kodeFilm, String judulFilm) {
        Films film = filmsRepository.findByKodeFilm(kodeFilm);
        film.setJudulFilm(judulFilm);
        filmsRepository.save(film);
    }

    @Override
    @Transactional
    public void hapusFilm(String kodeFilm) {
    filmsRepository.hapusFilm(kodeFilm);
    }

    @Override
    public void saveSchedule(Films kodeFilm, String tanggalTayang, String jamMulai, String jamSelesai, Float hargaTiket) {
        Schedules schedule = new Schedules();
        schedule.setKodeFilm(kodeFilm);
        schedule.setTanggalTayang(tanggalTayang);
        schedule.setJamMulai(jamMulai);
        schedule.setJamSelesai(jamSelesai);
        schedule.setHargaTiket(hargaTiket);
        schedulesRepository.save(schedule);
    }

    @Override
    public List<Films> getFilmsBySedangTayang(String sedangTayang) {
        return filmsRepository.findFilmsBySedangTayang(sedangTayang);
    }

    @Override
    public List<Schedules> getJadwalFilm(String judulFilm) {
        return schedulesRepository.findJadwalFilm(judulFilm);
    }
}
